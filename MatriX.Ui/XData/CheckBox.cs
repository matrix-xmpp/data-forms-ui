﻿using System.Drawing;
using System.Windows.Forms;

namespace Matrix.Ui.XData
{
    internal partial class CheckBox : Field
    {
        private readonly System.Windows.Forms.CheckBox checkBox;
        private readonly System.Windows.Forms.Label label;

        public CheckBox()
        {
            InitializeComponent();

            checkBox = new System.Windows.Forms.CheckBox();
            label = new System.Windows.Forms.Label();
            SuspendLayout();

            label.TextAlign = ContentAlignment.MiddleLeft;
            label.BackColor = Color.Transparent;
           
            checkBox.FlatStyle = FlatStyle.System;
           
            panelLeft.Controls.Add(label);
            panelLeft.Controls.Add(checkBox);
            Resize += XDataCheckBox_Resize;
            ResumeLayout(false);
        }

        public bool Value
        {
            get { return checkBox.Checked; }
            set { checkBox.Checked = value; }
        }

        public override string Text
        {
            get { return label.Text; }
            set { label.Text = value; }
        }
        
        public override bool Validate()
        {
            // a checkbox needs no validation because it can't be wrong
            return true;
        }

        private void XDataCheckBox_Resize(object sender, System.EventArgs e)
        {
            label.Location = new Point(0, 0);
            label.Width = label.Parent.Width / 3;
            int lblHeight = GetTextHeight(label);
            label.Height = lblHeight > checkBox.Height ? lblHeight : checkBox.Height;

            checkBox.Location = new Point(label.Width, 0);
            checkBox.Width = checkBox.Parent.Width - label.Width;

            Height = checkBox.Height + H_SPACE;
        }
    }
}