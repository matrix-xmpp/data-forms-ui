﻿using System.Drawing;
using System.Windows.Forms;

namespace Matrix.Ui.XData
{
    internal partial class Field : UserControl
    {
        internal const int H_SPACE          = 6;
        internal const int MULTILINE_HEIGHT = 72;

        public Field()
        {
            InitializeComponent();
        }

        #region << Properties >>
        /// <summary>
        /// Gets or sets the var.
        /// </summary>
        /// <value>The var.</value>
        public string Var {get;set;}
       
        /// <summary>
        /// Is this a required field?
        /// Show an icon nexr to all required fields
        /// </summary>
        public bool IsRequired
        {
            get { return picRequired.Visible; }
            set { picRequired.Visible = value; }
        }
        #endregion

        internal int GetTextHeight(Control ctl)
        {
            var textSize = ctl.CreateGraphics().MeasureString(ctl.Text, ctl.Font, ctl.Width, StringFormat.GenericDefault);
            return (int)(textSize.Height);
        }

        public virtual new bool Validate()
        {
            return false;
        }
    }
}