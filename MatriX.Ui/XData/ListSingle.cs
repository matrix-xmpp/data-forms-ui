﻿using System;
using System.Drawing;

namespace Matrix.Ui.XData
{
    internal partial class ListSingle : Field
    {
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.ComboBox comboBox;

        public ListSingle()
        {
            InitializeComponent();

            // TODO: Add any initialization after the InitializeComponent call
            label = new System.Windows.Forms.Label();
            comboBox = new System.Windows.Forms.ComboBox();
            SuspendLayout();

            // 
            // label
            // 			
            label.TextAlign = ContentAlignment.MiddleLeft;
            label.BackColor = Color.Transparent;
            // 
            // comboBox
            // 
            comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            // 
            // XDataListSingle
            // 
            panelLeft.Controls.Add(comboBox);
            panelLeft.Controls.Add(label);
            Resize += XDataListSingle_Resize;
            ResumeLayout(false);
        }

        public override string Text
        {
            get { return label.Text; }
            set { label.Text = value; }
        }

        public string Value
        {
            get
            {
                var li = (ListItem)comboBox.SelectedItem;
                return li.Value;
            }
            set
            {
                for (int i = 0; i < comboBox.Items.Count; i++)
                {
                    var li = comboBox.Items[i] as ListItem;
                    if (li.Value == value)
                    {
                        comboBox.SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        public void AddValue(string val, string lbl)
        {
            var item = new ListItem(lbl, val);
            comboBox.Items.Add(item);
        }

        public override bool Validate()
        {
            return true;
        }

        private void XDataListSingle_Resize(object sender, System.EventArgs e1)
        {
            label.Location = new Point(0, 0);
            label.Width = label.Parent.Width / 3;
            int lblHeight = GetTextHeight(label);
            label.Height = lblHeight > comboBox.Height ? lblHeight : comboBox.Height;

            comboBox.Location = new Point(label.Width, 0);
            comboBox.Width = comboBox.Parent.Width - label.Width;

            Height = Math.Max(label.Height, comboBox.Height) + H_SPACE;
        }
    }
}