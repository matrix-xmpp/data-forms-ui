﻿namespace Matrix.Ui.XData
{
    partial class Field
    {
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }


        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelRight = new System.Windows.Forms.Panel();
            this.picRequired = new System.Windows.Forms.PictureBox();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.errorProv = new System.Windows.Forms.ErrorProvider(this.components);
            this.panelRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProv)).BeginInit();
            this.SuspendLayout();
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.picRequired);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelRight.Location = new System.Drawing.Point(0, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(20, 64);
            this.panelRight.TabIndex = 0;
            // 
            // picRequired
            // 
            this.picRequired.ErrorImage = null;
            this.picRequired.Image = global::MatriX.Ui.Properties.Resources.required;
            this.picRequired.Location = new System.Drawing.Point(2, 2);
            this.picRequired.Name = "picRequired";
            this.picRequired.Size = new System.Drawing.Size(16, 16);
            this.picRequired.TabIndex = 0;
            this.picRequired.TabStop = false;
            this.picRequired.Visible = false;
            // 
            // panelLeft
            // 
            this.panelLeft.BackColor = System.Drawing.SystemColors.Control;
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(20, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(125, 64);
            this.panelLeft.TabIndex = 1;
            // 
            // errorProv
            // 
            this.errorProv.BlinkRate = 1000;
            this.errorProv.ContainerControl = this;
            // 
            // Field
            // 
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Name = "Field";
            this.Size = new System.Drawing.Size(145, 64);
            this.panelRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProv)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion


        internal System.Windows.Forms.Panel panelRight;
        internal System.Windows.Forms.Panel panelLeft;

        private System.Windows.Forms.PictureBox picRequired;
        internal System.Windows.Forms.ErrorProvider errorProv;
    }
}