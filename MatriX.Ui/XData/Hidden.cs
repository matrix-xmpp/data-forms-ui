﻿namespace Matrix.Ui.XData
{
    internal partial class Hidden : Field
    {
        public Hidden()
        {
            InitializeComponent();
        }

        #region << Properties >>

        public string Value { get; set; }

        #endregion

        public override bool Validate()
        {
            // a hidden fields needs no validation because it cant be wrong
            return true;
        }

        private void XDataHidden_Resize(object sender, System.EventArgs e)
        {
            this.Height = 0;
        }
    }
}
