﻿using System.Drawing;

namespace Matrix.Ui.XData
{
    internal partial class Label : Field
    {
        private readonly System.Windows.Forms.Label label;

        public Label()
        {
            InitializeComponent();

            label = new System.Windows.Forms.Label();
            SuspendLayout();
            label.UseMnemonic = false;
            label.BackColor = Color.Transparent;
            panelLeft.Controls.Add(label);
            Resize += XDataLabel_Resize;
            ResumeLayout(false);
        }

        public string Value
        {
            get { return label.Text; }
            set { label.Text = value; }
        }

        private void XDataLabel_Resize(object sender, System.EventArgs e1)
        {
            label.Location = new Point(0, 0);
            label.Width = label.Parent.Width;
            label.Height = GetTextHeight(label);

            Height = label.Height + H_SPACE;
        }

        public override bool Validate()
        {
            return true;
        }
    }
}