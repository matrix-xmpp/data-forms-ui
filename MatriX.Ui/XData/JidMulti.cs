﻿namespace Matrix.Ui.XData
{
    internal partial class JidMulti : TextBox
    {
        public JidMulti()
        {
            InitializeComponent();

            Multiline = true;
            ResumeLayout(false);
        }

        public override bool Validate()
        {
            // TODO, validae Jids
            if (IsRequired && Value == null)
            {
                errorProv.SetError(textBox, "field is required.");
                return false;
            }
            errorProv.SetError(textBox, "");
            return true;
        }
    }
}
