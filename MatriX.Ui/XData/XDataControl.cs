﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using Matrix.Xmpp.XData;

namespace Matrix.Ui.XData
{
    public partial class XDataControl : UserControl
    {
        /// <summary>
        /// horinzontal space between title and instructions
        /// </summary>
        private const int H_SPACE = 6;
        /// <summary>
        /// Space between instructions and the beginning of the fields.
        /// </summary>
        private const int H_SPACE_FIELDS = 6;


        public XDataControl()
        {
            InitializeComponent();
        }


        
        /// <summary>
        /// Property which could be used to store the sender of this form
        /// </summary>
        public Jid From { get; set; }

        

        
        public event EventHandler OnCancel;
        public event EventHandler OnOk;
        

        public bool ShowLegend
        {
            get { return panelLegend.Visible; }
            set { panelLegend.Visible = value; }
        }

        public bool ShowButtons
        {
            get { return panelButtons.Visible; }
            set { panelButtons.Visible = value; }
        }

        /// <summary>
        /// Creates the Xdata from from the given XData Element
        /// </summary>
        /// <param name="xdata"></param>
        public void CreateForm(Data xdata)
        {
            SuspendLayout();
			
            panelFields.SuspendLayout();
            panelFields.Controls.Clear();			
			
            lblTitle.Text			= xdata.Title;
            lblInstructions.Text	= xdata.Instructions;
			
            XDataControl_Resize(this, null);

            
            Xmpp.XData.Field[] fields = xdata.GetFields().ToArray();
            for (int i = fields.Length -1; i >= 0; i--)
            {				
                CreateField(fields[i], i);
            }
			
            panelFields.ResumeLayout();
			
            ResumeLayout();
        }

        //public void CreateForm(agsXMPP.protocol.iq.search.Search search)
        //{
        //    /*
        //     * Send: <iq xmlns="jabber:client" id="agsXMPP_9" type="get" to="users.jabber.org"><query xmlns="jabber:iq:search" />
        //     * </iq> 
        //        Recv: <iq xmlns="jabber:client" to="gnauck@amessage.de/SharpIM" type="result" id="agsXMPP_9" from="users.jabber.org">
        //     * <query xmlns="jabber:iq:search">
        //     * <instructions>Find a contact by entering the search criteria in the given fields. Note: Each field supports wild card searches (%)</instructions>
        //     * <first />
        //     * <last />
        //     * <nick />
        //     * <email />
        //     * </query></iq> 
        //     */
            
        //    this.SuspendLayout();

        //    this.panelFields.SuspendLayout();
        //    this.panelFields.Controls.Clear();

        //    //lblTitle.Text = xdata.Title;
        //    lblInstructions.Text =  search.Instructions;

        //    XDataControl_Resize(this, null);

        //    agsXMPP.Xml.Dom.ElementList list = new agsXMPP.Xml.Dom.ElementList();
        //    foreach (agsXMPP.Xml.Dom.Node n in search.ChildNodes)
        //    {
        //        if (n.NodeType == agsXMPP.Xml.Dom.NodeType.Element)
        //            list.Add(n as agsXMPP.Xml.Dom.Element);                
        //    }

        //    for (int i = list.Count -1; i >= 0; i--)
        //    {                 
        //        agsXMPP.Xml.Dom.Element el = list.Item(i);
        //        if (el.TagName == "key")
        //        {
        //            CreateField(CreateDummyHiddenField(el.TagName, el.Value), i);
        //        }
        //        else if (el.TagName == "instructions")
        //        {

        //        }
        //        else
        //        {
        //            CreateField(CreateDummyTextField(el.TagName, el.TagName), i);
        //        }
                
        //    }
           
        //    this.panelFields.ResumeLayout();

        //    this.ResumeLayout();
        //}

        /// <summary>
        /// helper function that maps old jabber search and register fields to our XData fields
        /// </summary>
        /// <param name="var"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        private Xmpp.XData.Field CreateDummyTextField(string var, string label)
        {
            return new Xmpp.XData.Field(var, label, FieldType.TextSingle);
        }

        /// <summary>
        /// helper function that maps old jabber hidden search and register fields to our XData fields
        /// </summary>
        /// <param name="var"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        private Matrix.Xmpp.XData.Field CreateDummyHiddenField(string var, string val)
        {
            var field = new Matrix.Xmpp.XData.Field(var, var, FieldType.Hidden);
            field.SetValue(val);
            return field;
        }

        /// <summary>
        /// Create a form element from a given field
        /// </summary>
        /// <param name="field"></param>
        /// <param name="tabIdx"></param>
        /// <returns></returns>
        private Field CreateField(Matrix.Xmpp.XData.Field field, int tabIdx)
        {		
            Field mData;
//			panelFields.SuspendLayout();

            FieldType mType = field.Type;
            string lbl;
            switch (mType)
            {
                case FieldType.Hidden:
                    mData = new Hidden();
                    ((Hidden) mData).Value		= field.GetValue();
					
                    break;
                case FieldType.Fixed:
                    mData = new Label();
					
                    ((Label) mData).Value		= field.GetValue();			
					
                    break;
                case FieldType.Boolean:
                    mData = new CheckBox();
					
                    lbl = field.Label;
                    if (lbl == null)
                        lbl = field.Var;
					
                    mData.Text	= lbl;
                    ((CheckBox) mData).Value	= field.GetValueBool();					
											
                    break;				
                case FieldType.TextPrivate:
                    mData = new TextBox();
					
                    lbl = field.Label;
                    if (lbl == null)
                        lbl = field.Var;

                    mData.Text			= lbl;
                    ((TextBox) mData).Value		= field.GetValue();
                    ((TextBox) mData).IsPrivate	= true;
					
                    break;
                case FieldType.TextSingle:
                    mData = new TextBox();
					
                    lbl = field.Label;
                    if (lbl == null)
                        lbl = field.Var;

                    mData.Text			= lbl;
                    ((TextBox) mData).Value		= field.GetValue();
					
                    break;
                case FieldType.TextMulti:
                    mData = new TextBox();
					
                    lbl = field.Label;
                    if (lbl == null)
                        lbl = field.Var;

                    ((TextBox) mData).Text			= lbl;
                    ((TextBox) mData).Multiline	= true;
                    ((TextBox) mData).Lines		= field.GetValues().ToArray();										
					
                    break;				
                case FieldType.ListSingle:
                    mData = new ListSingle();

                    lbl = field.Label;
                    if (lbl == null)
                        lbl = field.Var;

                    mData.Text		= lbl;
					
                    foreach(Option o in field.GetOptions())
                    {
                        ((ListSingle) mData).AddValue(o.Value, o.Label);
                    }					
                    ((ListSingle) mData).Value		= field.GetValue();
					
                    break;
                case FieldType.ListMulti:
                    mData = new ListMulti();
					
                    lbl = field.Label;
                    if (lbl == null)
                        lbl = field.Var;
					
                    mData.Text		= lbl;
					
                    foreach(Option o in field.GetOptions())
                    {
                        string optionValue = o.Value;
                        CheckState chk;
                        chk = field.HasValue(optionValue) ? CheckState.Checked : CheckState.Unchecked;								
						
                        ((ListMulti) mData).AddValue(optionValue, o.Label, chk);
                    }

                    break;
                case FieldType.JidSingle:
                    mData = new JidSingle();

                    lbl = field.Label;
                    if (lbl == null)
                        lbl = field.Var;

                    mData.Text	= lbl;
                    ((JidSingle) mData).Value	= field.Value;
                    break;
                case FieldType.JidMulti:
                    mData = new JidMulti();
					
                    lbl = field.Label;
                    if (lbl == null)
                        lbl = field.Var;

                    mData.Text	= lbl;
                    ((JidMulti) mData).Lines	= field.GetValues().ToArray();
					
                    break;
                default:
                    mData = new Label();

                    break;
            }
            mData.IsRequired	= field.Required;
            mData.Var			= field.Var;
            mData.Parent		= panelFields;
            mData.Location		= new Point(0, 0);
            mData.Width			= panelFields.Width;			
            mData.Dock			= DockStyle.Top;
			
            if (mType != FieldType.Hidden && 
                mType != FieldType.Fixed)
            {
                mData.TabIndex	= tabIdx;
                mData.Focus();
            }


            panelFields.Controls.Add(mData);

//			panelFields.ResumeLayout();
            //lbl = null;
            return mData;
        }

        internal int GetTextHeight(Control ctl)
        {
            SizeF textSize = ctl.CreateGraphics().MeasureString(ctl.Text, ctl.Font, ctl.Width, StringFormat.GenericDefault);
            return (int)(textSize.Height);
        }
		
        private void XDataControl_Resize(object sender, System.EventArgs e)
        {
            lblTitle.Height = GetTextHeight(lblTitle) + H_SPACE;
            lblInstructions.Height = GetTextHeight(lblInstructions);
            panelTop.Height = lblTitle.Height + lblInstructions.Height  + H_SPACE_FIELDS;
        }

        private void cmdOK_Click(object sender, System.EventArgs e)
        {
            if( Validate() )
            {
                Console.WriteLine(CreateResponse().ToString());
                if (OnOk != null)
                    OnOk(this, e);
            }
        }

        /// <summary>
        /// Validates the xData from. You should call this function for validation 
        /// before submitting a response, and only submit if the result is true.
        /// </summary>
        /// <returns></returns>
        public new bool Validate()
        {
            int errCount = 0;
            foreach(Field ctl in panelFields.Controls)
            {
                if (!ctl.Validate())
                    errCount++;
            }
            return errCount == 0 ? true : false;
        }

        /// <summary>
        /// Creates the XData Response Element from the entered Data in the GUI Form
        /// </summary>
        /// <returns></returns>
        public Data CreateResponse()
        {
            var data = new Data(FormType.Submit);
            foreach(Field ctl in panelFields.Controls)
            {
                Type type = ctl.GetType();

                if ( type == typeof(TextBox) )
                {
                    Xmpp.XData.Field f;
                    var txt = ctl as TextBox;
                    if (txt.Multiline == true)
                    {
                        f = new Xmpp.XData.Field(FieldType.TextMulti);						
                    }	
                    else
                    {
                        if (txt.IsPrivate)
                            f = new Xmpp.XData.Field(FieldType.TextPrivate);
                        else
                            f = new Xmpp.XData.Field(FieldType.TextSingle);
						
                    }
                    f.AddValues(txt.Values);
                    f.Var = txt.Var;
                    data.AddField(f);

                }
                else if (type == typeof(Hidden))
                {
                    var hidden = ctl as Hidden;
					
                    var f = new Xmpp.XData.Field(FieldType.Hidden);
					
                    f.AddValue(hidden.Value);
                    f.Var = hidden.Var;
					
                    data.AddField(f);
                }
                else if ( type == typeof(JidSingle) )
                {
                    var jids = ctl as JidSingle;
                    var f = new Xmpp.XData.Field(FieldType.JidSingle);
                    f.SetValue(jids.Value);
                    f.Var = jids.Var;
                    data.AddField(f);
                }
                else if ( type == typeof(JidMulti) )
                {
                    var jidm = ctl as JidMulti;
                    var f = new Xmpp.XData.Field(FieldType.JidMulti);
                    f.AddValues(jidm.Values);
                    f.Var = jidm.Var;
                    data.AddField(f);
					
                }
                else if ( type == typeof(CheckBox) )
                {
                    var chk = ctl as CheckBox;
                    var f = new Xmpp.XData.Field(FieldType.Boolean);
                    f.SetValueBool(chk.Value);
                    f.Var = chk.Var;
                    data.AddField(f);
                }
                else if ( type == typeof(ListMulti) )
                {
                    var listm = ctl as ListMulti;
                    var f = new Xmpp.XData.Field(FieldType.ListMulti);					
                    f.AddValues(listm.Values);
                    f.Var = listm.Var;
                    data.AddField(f);
                }
                else if ( type == typeof(ListSingle) )
                {
                    var lists = ctl as ListSingle;
                    var f = new Xmpp.XData.Field(FieldType.ListSingle);					
                    f.SetValue(lists.Value);
                    f.Var = lists.Var;
                    data.AddField(f);
                }				
            }

            return data;
        }


        /// <summary>
        /// Build the response from a old jabber search form
        /// </summary>
        /// <returns></returns>
        //public agsXMPP.protocol.iq.search.Search CreateSearchResponse()
        //{
        //    agsXMPP.protocol.iq.search.Search search = new agsXMPP.protocol.iq.search.Search();
            
        //    foreach (Field ctl in panelFields.Controls)
        //    {
        //        Type type = ctl.GetType();

        //        if (type == typeof(XDataTextBox))
        //        {                    
        //            XDataTextBox txt = ctl as XDataTextBox;
        //            if (txt.Value != null)
        //            {
        //                search.SetTag(txt.Var, txt.Value);                        
        //            }
        //        }
        //        else if (type == typeof(XDataHidden))
        //        {
        //            XDataHidden hidden = ctl as XDataHidden;
        //            search.SetTag(hidden.Var, hidden.Value);
        //        }
        //    }

        //    return search;
        //}

        private void cmdCancel_Click(object sender, System.EventArgs e)
        {
            if (OnCancel != null)
                OnCancel(this, e);
        }		
    }
}
