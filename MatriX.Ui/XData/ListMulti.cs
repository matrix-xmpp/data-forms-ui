﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Matrix.Ui.XData
{
    internal partial class ListMulti : Field
    {
        internal System.Windows.Forms.Label label;
        private CheckedListBox checkedListBox;

        public ListMulti()
        {
            InitializeComponent();

            label = new System.Windows.Forms.Label();
            checkedListBox = new CheckedListBox();
            SuspendLayout();
            // 
            // label
            // 			
            label.TextAlign = ContentAlignment.TopLeft;
            label.BackColor = Color.Transparent;
            // 
            // checkedListBox
            // 
            checkedListBox.Height = MULTILINE_HEIGHT;
            // 
            // XDataListMulti
            // 
            panelLeft.Controls.Add(checkedListBox);
            panelLeft.Controls.Add(label);
            Resize += XDataListMulti_Resize;
            ResumeLayout(false);
        }

        public override string Text
        {
            get { return label.Text; }
            set { label.Text = value; }
        }

        public string[] Values
        {
            get
            {
                //checkedListBox
                int count = checkedListBox.CheckedItems.Count;
                var result = new string[count];
                for (int i = 0; i < count; i++)
                {
                    var li = checkedListBox.CheckedItems[i] as ListItem;
                    result[i] = li.Value;
                }
                return result;
            }
        }

        public void AddValue(string val, string lbl)
        {
            AddValue(val, lbl, CheckState.Unchecked);
        }

        public void AddValue(string val, string lbl, CheckState check)
        {
            var item = new ListItem(lbl, val);
            checkedListBox.Items.Add(item, check);
        }

        public override bool Validate()
        {
            return true;
        }

        private void XDataListMulti_Resize(object sender, System.EventArgs e1)
        {
            label.Location = new Point(0, 0);
            label.Width = label.Parent.Width / 3;
            int lblHeight = GetTextHeight(label);
            if (lblHeight > checkedListBox.Height)
                label.Height = lblHeight;
            else
                label.Height = checkedListBox.Height;

            checkedListBox.Location = new Point(label.Width, 0);
            checkedListBox.Width = checkedListBox.Parent.Width - label.Width;

            Height = Math.Max(label.Height, checkedListBox.Height) + H_SPACE;
        }
    }
}
