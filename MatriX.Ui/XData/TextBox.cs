﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Matrix.Ui.XData
{
    internal partial class TextBox : Field
    {
        internal System.Windows.Forms.TextBox textBox;
        private readonly System.Windows.Forms.Label label;

        public TextBox()
        {
            InitializeComponent();

            textBox = new System.Windows.Forms.TextBox();
            label = new System.Windows.Forms.Label();
            SuspendLayout();

            label.TextAlign = ContentAlignment.MiddleLeft;
            label.BackColor = Color.Transparent;

            panelLeft.Controls.Add(textBox);
            panelLeft.Controls.Add(label);
            Resize += XDataTextBox_Resize;
            ResumeLayout(false);

            errorProv.SetIconAlignment(textBox, ErrorIconAlignment.MiddleLeft);
            errorProv.BlinkStyle = ErrorBlinkStyle.AlwaysBlink;
        }

        public bool Multiline
        {
            get { return textBox.Multiline; }
            set
            {
                if (value)
                {
                    textBox.Multiline = true;
                    textBox.Height = MULTILINE_HEIGHT;
                    textBox.ScrollBars = ScrollBars.Vertical;
                    label.TextAlign = ContentAlignment.TopLeft;
                }
                else
                {
                    textBox.Multiline = false;
                    label.TextAlign = ContentAlignment.MiddleLeft;
                }
            }
        }

        public override string Text
        {
            get { return label.Text; }
            set { label.Text = value; }
        }

        public string Value
        {
            get
            {
                if (textBox.Text.Length > 0)
                    return textBox.Text;
                return null;
            }
            set { textBox.Text = value; }
        }

        public string[] Values
        {
            get { return textBox.Lines; }
            set { textBox.Lines = value; }
        }

        /// <summary>
        /// is this a password char textbox? (private)
        /// </summary>
        public bool IsPrivate
        {
            get { return textBox.PasswordChar == 0 ? false : true; }
            set
            {
                if (value)
                    textBox.PasswordChar = '*';
                else
                    textBox.PasswordChar = (char)0;
            }
        }

        public string[] Lines
        {
            get { return textBox.Lines; }
            set { textBox.Lines = value; }
        }

        private void XDataTextBox_Resize(object sender, System.EventArgs e1)
        {
            label.Location = new Point(0, 0);
            label.Width = label.Parent.Width / 3;
            int lblHeight = GetTextHeight(label);
            label.Height = lblHeight > textBox.Height ? lblHeight : textBox.Height;

            textBox.Location = new Point(label.Width, 0);
            textBox.Width = textBox.Parent.Width - label.Width;

            Height = Math.Max(label.Height, textBox.Height) + H_SPACE;
        }

        public override bool Validate()
        {
            if (IsRequired && Value == null)
            {
                errorProv.SetError(textBox, "field is required.");
                return false;
            }
            errorProv.SetError(textBox, "");
            return true;
        }
    }
}