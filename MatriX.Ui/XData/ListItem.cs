using System;

namespace Matrix.Ui.XData
{
    /// <summary>
    /// Summary description for XDataListItem.
    /// </summary>
    internal class ListItem : Object
    {
        public ListItem()
        {		
        }
		
        public ListItem(string label, string val)
        {
            Value = val;
            Label	= label;
        }

        public string Label { get; set; }

        public string Value { get; set; }

        public override string ToString()
        {
            return Label;
        }
    }
}