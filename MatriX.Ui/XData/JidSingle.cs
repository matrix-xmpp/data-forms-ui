﻿namespace Matrix.Ui.XData
{
    internal partial class JidSingle : TextBox
    {
        public JidSingle()
        {
            InitializeComponent();
        }

        public override bool Validate()
        {
            // TODO, validate Jids
            if (IsRequired && Value == null)
            {
                errorProv.SetError(textBox, "field is required.");
                return false;
            }
            errorProv.SetError(textBox, "");
            return true;
        }
    }
}